import { useState } from "react";
import CounterSizeGenerator from "./CounterSizeGenerator";
import CounterGroup from "./CounterGroup";
import CountGroupSum from "./CountGroupSum";

const MultipleCounter = () => {
    // const [size, setSize] = useState(0)
    const [counterList, setCounterList] = useState([])

    const updateCounterList = (item, value) => {
        const newCounterList = [...counterList]
        newCounterList[item] = value
        setCounterList(newCounterList)
    }

    const changeCounterList = (size) => {
        setCounterList(Array(size).fill(0))
    }


    const sum = counterList.reduce((sum, counter) => {
            return sum + counter;
        }, 0)



    return (
        <div>
            <CounterSizeGenerator changeCounterList={changeCounterList}></CounterSizeGenerator>
            <CountGroupSum sum={sum}></CountGroupSum>
            <CounterGroup counterList={counterList} updateCounterList={updateCounterList}></CounterGroup>
        </div>
    )
}

export default MultipleCounter