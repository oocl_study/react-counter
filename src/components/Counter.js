import { useState } from "react";

const Counter = ({item, value, updateCounter}) => {


    const increaseCount = () => {
        updateCounter(item, value + 1)
    }

    
    const decreaseCount = () => {
        updateCounter(item, value - 1)
    }

    return (
        <div>
            <button onClick = {increaseCount}>+</button>
            <span >{value}</span>
            <button onClick = {decreaseCount}>-</button>
        </div>
    )
}

export default Counter;