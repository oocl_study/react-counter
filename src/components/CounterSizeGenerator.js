import { updateParenthesizedType } from "typescript";

const CounterSizeGenerator = ({changeCounterList}) => {

    const updateSize = (event) => {
        changeCounterList(Number(event.target.value))
    }

    return (
        <div>
            <input type="number" name="countersize" onChange={updateSize}></input>
        </div>
    )

}
export default CounterSizeGenerator