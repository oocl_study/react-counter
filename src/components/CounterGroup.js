import Counter from "./Counter";
import { updateCommaList } from "typescript";

const CounterGroup = ({counterList, updateCounterList}) => {

    const updateCounter = (item, value) => {
        updateCounterList(item, value)
    }

    return (
        <div>
            {counterList.map((value,index) => {
                return <Counter key={index} item = {index} value = {value} updateCounter = {updateCounter}></Counter>
            })}
        </div>
    )
}

export default CounterGroup