import logo from './logo.svg';
import './App.css';
import Counter from './components/Counter';
import CounterGroup from './components/CounterGroup';
import MultipleCounter from './components/MultipleCounter';

function App() {
  return (
    <div className="App">
      <MultipleCounter></MultipleCounter>
    </div>
  );
}

export default App;
